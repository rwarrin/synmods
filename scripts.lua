local function RemoveGryphons()
    MainMenuBarArtFrame.LeftEndCap:Hide();
    MainMenuBarArtFrame.RightEndCap:Hide();
end

local function SynModsCastingBar_Mod()
	CastingBarFrame:ClearAllPoints();
	CastingBarFrame:SetScale(2.0);
	CastingBarFrame:SetPoint("BOTTOM", UIParent, "BOTTOM", 0, 70);
	CastingBarFrame.SetPoint = function() end

	CastingBarFrame.Border:SetTexture(nil);
	CastingBarFrame.Flash:SetTexture(nil);

	CastingBarFrame.Text:ClearAllPoints();
	CastingBarFrame.Text:SetPoint("CENTER", "CastingBarFrame", "CENTER", 0, 0);
	CastingBarFrame.Text.SetPoint = function () end
	--[[
    _G["CastingBarFrame"].Border:SetTexture(nil);
    _G["CastingBarFrame"].Flash:SetTexture(nil);
    _G["CastingBarFrame"]:SetWidth(CastingBarFrame:GetWidth() + 140);
    _G["CastingBarFrame"]:SetHeight(CastingBarFrame:GetHeight() + 20);
    _G["CastingBarFrame"]:ClearAllPoints();
    _G["CastingBarFrame"]:SetPoint("BOTTOM", UIParent, "BOTTOM", 0, 120);
    _G["CastingBarFrame"]:SetPoint("BOTTOM", UIParent, "BOTTOM", 0, 82);
    _G["CastingBarFrame"].SetPoint = function() end
    _G["CastingBarFrame"].Text:ClearAllPoints();
    _G["CastingBarFrame"].Text:SetPoint("LEFT", CastingBarFrame, "LEFT", -40, 0);
    _G["CastingBarFrame"].Text.SetPoint = function() end
    _G["CastingBarFrame"].Text:SetFont("Fonts\\ARILAN.ttf", 16, "THINOUTLINE");

    -- create timer text
    _G["CastingBarFrame"].timer = _G["CastingBarFrame"]:CreateFontString(nil);
    _G["CastingBarFrame"].timer:SetFont("Fonts\\ARIALN.ttf", 16, "THINOUTLINE");
    _G["CastingBarFrame"].timer:SetPoint("RIGHT", _G["CastingBarFrame"], "RIGHT", -10, 0);
    _G["CastingBarFrame"].update = .1;
	]]--
end

local function SynModsCastingBar_OnUpdate(self, elapsed)
    if not self.timer then return end
    if self.update and self.update < elapsed then
        if self.casting then
            self.timer:SetText(format("%.1f", max(self.maxValue - self.value, 0)));
        elseif self.channeling then
            self.timer:SetText(format("%.1f", max(self.value, 0)));
        else
            self.timer:SetText("");
        end
        self.update = .1
    else
        self.update = self.update - elapsed
    end
end

local function ResizePetActionBar()
	PetActionBarFrame:SetScale(0.8);
    PetActionBarFrame:ClearAllPoints();
	PetActionBarFrame:SetPoint("BOTTOMLEFT", "MultiBarBottomLeft", "TOPLEFT", -30, 20);
    PetActionBarFrame.SetPoint = function () end
end

local function SetPlayerFrame()
	PlayerFrame:SetScale(1.3);
	PlayerFrame:ClearAllPoints();
	PlayerFrame:SetPoint("RIGHT", UIParent, "CENTER", -110, -170);
	PlayerFrame.SetPoint = function () end
	--[[
    _G["PlayerFrame"]:ClearAllPoints();
    _G["PlayerFrame"]:SetPoint("TOP", UIParent, "Center", -230, -150);
    _G["PlayerFrame"]:SetScale(1.3);
    _G["PlayerFrameTexture"]:SetVertexColor(0.1, 0.1, 0.1, 0.90);
	]]--
end

local function SetTargetFrame()
	TargetFrame:SetScale(1.3);
	TargetFrame:ClearAllPoints();
	TargetFrame:SetPoint("LEFT", UIParent, "CENTER", 110, -170);
	TargetFrame.SetPoint = function () end
	--[[
	local TargetFrameTexture = _G["TargetFrameTextureFrameTexture"];
	TargetFrameTexture:SetVertexColor(0.1, 0.1, 0.1, 0.90);
    _G["TargetFrame"]:ClearAllPoints();
    _G["TargetFrame"]:SetPoint("TOP", UIParent, "Center", 230, -150);
    _G["TargetFrame"]:SetScale(1.3);
    _G["TargetFrameTextureFrameTexture"]:SetVertexColor(0.1, 0.1, 0.1, 0.90);
	]]--
end

local function SetFocusFrame()
	FocusFrame:ClearAllPoints();
	FocusFrame:SetPoint("BOTTOM", "PlayerFrame", "TOPLEFT", 0, -30);
	--[[
	FocusFrameTexture:SetVertexColor(0.1, 0.1, 0.1, 0.90);
    _G["FocusFrame"]:ClearAllPoints();
    _G["FocusFrame"]:SetPoint("LEFT", UIParent, "LEFT", 420, -170);
    _G["FocusFrameTextureFrameTexture"]:SetVertexColor(0.1, 0.1, 0.1, 0.90);
	]]--
end

local function SetPetFrame()
    PetFrame:ClearAllPoints();
    PetFrame:SetPoint("LEFT", PlayerFrame, "LEFT", 116, 58);
    PetFrame:SetScale(0.75);
    PetFrame.SetPoint = function() end
end


local function ScaleMenuBar()
    _G["MainMenuBar"]:SetScale(0.85);
end

local function ScaleTargetCastBar()
    _G["TargetFrameSpellBar"]:SetScale(1.4)
end

local function RemoveUIErrorsFrame()
    _G["UIErrorsFrame"]:Hide();
end

local function MoveBuffFrame()
	BuffFrame:ClearAllPoints();
	BuffFrame:SetPoint("TOPRIGHT", "MinimapCluster", "TOPLEFT", 0, -10);
	BuffFrame.SetPoint = function() end
end

local function ScaleMinimap()
	MinimapCluster:SetScale(1.3);

	MultiBarRight:SetScale(0.85);
	MultiBarRight.SetScale = function () end
	MultiBarRight:ClearAllPoints();
	MultiBarRight:SetPoint("TOPRIGHT", "MinimapCluster", "BOTTOMRIGHT", 0, 0);
	MultiBarRight.SetPoint = function () end

	MultiBarLeft:SetScale(0.85);
	MultiBarLeft.SetScale = function () end
	MultiBarLeft:ClearAllPoints();
	MultiBarLeft:SetPoint("TOPRIGHT", "MultiBarRight", "BOTTOMRIGHT", 0, 0);
	MultiBarLeft.SetPoint = function () end
end

local function SetupChatWindows()
	for WindowIndex = 1, NUM_CHAT_WINDOWS do
		local ChatFrame = _G["ChatFrame"..WindowIndex];
		ChatFrame:SetPoint("BOTTOMLEFT", UIParent, "BOTTOMLEFT", 0, 145);
		ChatFrame:SetWidth(375);
		ChatFrame:SetHeight(127);
		ChatFrame:SetFading(false);

		local FrameName = ChatFrame:GetName();
		_G["ChatFrame"..WindowIndex.."EditBoxLeft"]:SetTexture(nil);
		_G["ChatFrame"..WindowIndex.."EditBoxMid"]:SetTexture(nil);
		_G["ChatFrame"..WindowIndex.."EditBoxRight"]:SetTexture(nil);

	end
end

local function ObjectiveTrackerPositioning()
	ObjectiveTrackerFrame:SetPoint("TOPRIGHT", "MultiBarRight", "TOPLEFT", -5, 0);
	ObjectiveTrackerFrame:SetScale(0.9);
	ObjectiveTrackerFrame:HookScript("OnEvent",
		function(self)
			self:SetPoint("TOPRIGHT", "MultiBarRight", "TOPLEFT", -5, 0);
			self:SetScale(0.9);
			self.SetPoint = function () end
		end
	);
end

local function OnEvent(self, event, ...)
    if(event == "PLAYER_LOGIN") then
        RemoveGryphons();
        SetPlayerFrame();
        SetTargetFrame();
        SetFocusFrame();
        SetPetFrame();
        --ScaleMenuBar();
        ScaleTargetCastBar();
        SynModsCastingBar_Mod();
		ResizePetActionBar();
        RemoveUIErrorsFrame();
		ScaleMinimap();
		MoveBuffFrame();
		SetupChatWindows();
		ObjectiveTrackerPositioning();
        hooksecurefunc("CastingBarFrame_OnUpdate", SynModsCastingBar_OnUpdate);
    end
end


synModsFrame = CreateFrame("Frame", "SynMods", nil);
synModsFrame:RegisterEvent("PLAYER_LOGIN");
synModsFrame:SetScript("OnEvent", OnEvent);



